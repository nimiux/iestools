#! /bin/sh
### BEGIN INIT INFO
# Provides:          invitado
# Required-Start:    mountall
# Required-Stop:
# Default-Start:     5
# Default-Stop:
# Short-Description: Sets up invitado home dir
# Description:
### END INIT INFO

rsync -azt /root/invitado/ /home/invitado/
