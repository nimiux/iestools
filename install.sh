#!/bin/bash


PROJDIR=$(dirname $0)
MAKEFILE=Makefile.PL
[[ ! -d $PROJDIR ]] && echo "Invalid directory: $PROJDIR" && exit 1
[[ ! -f $PROJDIR/$MAKEFILE ]] && echo "No Makefile.PL found in directory $PROJDIR dir" && exit 2
cd $PROJDIR
perl Makefile.PL
make
make test
#make manifest
#make dist
make install

