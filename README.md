[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

IESTools version 1.0
====================

Herramientas de apoyo a tareas docentes

INSTALLATION

Para instalar este módulo:

1. Instalar ssh y sshpass
2. Instalar cpan (normalmente se entrega con Perl)
3. $ cpan Archive::Extract File::Copy::Recursive Getopt::Long::Descriptive List::AllUtils Text::CSV
4. Instalar CoreSubs desde el repositorio de nimiux:
    $ git clone https://codeberg.org/nimiux/coresubs
    $ cd coresubs
    $ ./install.sh
5. Lanzar install.sh

DEPENDENCIES

Text::CSV
CAM::PDF
#Archive::Extract File::Copy::Recursive Getopt::Long::Descriptive List::AllUtils

USAGE

Utilidades en el directorio bin:

compruebaficherovbox.pl  Comprueba si un fichero de definición de máquina VB se adapta a ciertas restricciones
enviamensaje.sh          Envía mensajes a un grupo de alumnos
genpasswdfile.pl         Genera un fichero de definición de cuentas de usuario para sistemas *nix
maquinasaula.pl          Gestiona comunicaciones con máquinas en un aula
rellenapdf               Rellena los campos de un documento PDF (Se trata de una prueba de concepto para desarrollar todo el
                         procedimiento de comunicación de documentos al alumno
restorembz.pl            Restaura en el sistema de ficheros un curso de Moodle
tareamoodle.pl           Desempaqueta una tarea Moodle, comprueba su estructura y muestra el contenido del material entregado

SEE ALSO

El formato MAT (Moodle Assignment Template)

Este formato define la estructura, tipo y funciones ayudante para comprobar el
material entregado en una tarea entregada a través del LMS Moodle. Se trata de
un Hash escrito en el formato del lenguaje de programación Perl con la
siguiente estructura:

Cada elemento del hash es un elemento que se espera encontrar en la tarea de
Moodle una vez desempaquetada, para ello, se define un segundo nivel de hash
con las claves:

type:      Indica el tipo de fichero que se espera sea el elemento (folder,
           txt, png, pdf, ...)
contents:  Para carpetas se define un nuevo nivel de hash en el que se define
           de nuevo el contenido esperado. Para los ficheros se define la
           función que se evaluará para tratar este elemento (esta función
           admite dos argumentos: La ruta al fichero y el tipo de fichero)

El siguiente ejemplo se encuentra en la carpeta etc/templates/sample.mat del
proyecto.

    {
        'carpeta' => {
            'type' => 'folder',
            'contents' => {
                'foto.png' => {
                    'type' => 'png',
                    'contents' => \&showfile
                },
            },
        },
        'prueba.txt' => {
            'type' => 'txt',
            'contents' => \&showfile,
        },
        'document.pdf' => {
            'type' => 'pdf',
            'contents' => \&showfile,
        },
    }

Los tipos admitidos se pueden consultar en el hash referenciado por $mimes en
el módulo CoreSubs.

COPYRIGHT AND LICENCE

Copyright (c) 2018-2025 by José María Alonso Josa <nimiux@freeshell.de>

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.
