#!/usr/bin/env perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.16';

use Cwd 'abs_path';
use Data::Dumper;
use Getopt::Long::Descriptive;
use File::Basename;
use Text::CSV;

use CoreSubs::CoreSubs qw(
    $DEBUG
        bluemsg
        debugmsg
    donemsg_
    failmsg_
    komsg
    myname
    uniq
);

use CoreSubs::Proc qw(
    loadperl
    runcommand
);

use CoreSubs::Storage qw(
    readhashfromcsv
);

my $home = $ENV{"HOME"};
my $CONFIGDIRVAR = "IESTOOLSCONFIGDIR";
my $configdir = $ENV{$CONFIGDIRVAR} || "";
komsg("Variable de entorno $CONFIGDIRVAR no definida"), exit if not $configdir;
my $configfile = "$configdir/rooms.pl";
my $NULL = "/dev/null";

=head2 HELPERS

=cut

sub msg {
    # flag:
    # 0 if no message should be printed
    # 1 if only fail message should be printed
    # 2 if done and fail message should be printed
    # nl:
    # 0 don't print newline
    # 1 print newline
    my ($result, $nl, $flag) = @_;
    my $msg = $result->{machineinfo};
    $nl = 1 if not defined $nl;
    $flag = 2 if not defined $flag;
    debugmsg("newline: $nl flag: $flag");
    if ($result->{code} eq 0) {
        donemsg_($msg) if ($flag == 2);
    } else {
        failmsg_($msg) if ($flag > 0);
    }
    print "\n" if $nl;
}

sub getmachineinfo {
    my ($machine, $config) = @_;
    my $ip = "$config->{net}.$machine->{room}.$machine->{number}";
    return "$machine->{machine} ($ip) [$machine->{first} $machine->{last}]";
}

sub runmachine {
    my ($machine, $command, $config) = @_;
    my $result = runcommand($command, $config);
    $result->{machineinfo} = getmachineinfo($machine, $config),
    msg($result);
}

sub runaction {
    my ($func, $hash, $config, $extra) = @_;
    #print("Run action");
    #print (Dumper \$hash );
    my @result;
    my $machine;
    foreach my $machinename (keys %$hash) {
        debugmsg("Running action for machine: $machinename");
        $machine = $hash->{$machinename};
        push @result, $func->($machine, $hash, $config, $extra);
    }
    return \@result;
}

# Action funcs
sub runaliases {
    my ($machine, $hash, $config, $extra) = @_;
    return "alias $machine->{machine}='${\(myname)} -d ssh -r $machine->{room} -m $machine->{machine}'";
}

sub runhostnames {
    my ($machine, $hash, $config, $extra) = @_;
    return "$config->{net}.$machine->{room}.$machine->{number} $machine->{machine}";
}

sub runinfo {
    my ($machine, $hash, $config, $extra) = @_;
    my $result = runremotecommand($machine, $config, 1, "uname -norms");
    my $osmsg = $result->{result};
    my $os = ( $osmsg =~ /^Linux/ ) ? $osmsg : "Other";
    if ( $result->{code} eq 0 ) {
        bluemsg("$os");
    } else {
        komsg("$result->{machineinfo}");
    }
}

sub runlist {
    my ($machine, $hash, $config, $extra) = @_;
    return getmachineinfo($machine, $config);
}

sub runping {
    my ($machine, $hash, $config, $extra) = @_;
    my $timeout = $config->{pingtimeout};
    my $count = $config->{pingcount};
    my $command = "ping -q -W $timeout -c $count $machine->{machine} > $NULL 2> $NULL";
    my $result = runcommand($command, $config);
    $result->{'machineinfo'} = getmachineinfo($machine, $config);
    return $result;
}

sub runscp {
    my ($machine, $hash, $config, $extra) = @_;
    my $command = "scp $extra > $NULL 2> $NULL";
    my $result = runcommand($command, $config);
    msg($result);
}

sub runssh {
    my ($machine, $hash, $config, $extra) = @_;
    $extra = "\"$extra\"";
    debugmsg("Commands: $extra");
    my $result = runremotecommand($machine, $config, 0, $extra);
    msg($result);
}

sub runtestssh {
    my ($machine, $hash, $config, $extra) = @_;
    my $timeout = $config->{pingtimeout};
    my $command = "ssh-keyscan -T $timeout $machine->{machine} > $NULL 2> $NULL";
    #my $command = "nc -z hostname 22";
    my $result = runcommand($command, $config);
    msg($result);
}

sub runremotecommand {
    my ($machine, $config, $callmode, $remotecommand) = @_;
    $remotecommand = $remotecommand || "";
    my $timeout = $config->{sshtimeout};
    my $hostcheck = $config->{hostcheck};
    my $machinename = $machine->{machine};
    my $user = $machine->{user};
    my $pass = $machine->{pass};
    debugmsg("Contraseña para $user: $pass") if $pass;
    my $sshpass = $pass ? "sshpass -p$pass" : "";
    my $command = "$sshpass ssh -o StrictHostKeyChecking=$hostcheck -o ConnectTimeOut=$timeout -l $user $machinename $remotecommand 2> $NULL";
    debugmsg("Lanzando orden: $command");
    my $result = runcommand($command, $config, $callmode);
    $result->{'machineinfo'} = getmachineinfo($machine, $config);
    return $result;
}

# Actions dispatcher
# TODO. Implement this dipatch in CoreSubs
my %actions = (
    'aliases' => \&aliases,
    'cpf' => \&cpf,
    'cpt' => \&cpt,
    'hostnames' => \&hostnames,
    'info' => \&info,
    'list' => \&list,
    'ping' => \&ping,
    'rooms' => \&rooms,
    'ssh' => \&ssh,
    'testssh' => \&testssh
);

#TODO: Clean
sub actions {
    my ($hash, $config, $extra) = @_;
    debugmsg("Listar acciones");
    say join "\n", sort keys %actions;
}

# Action funcs
sub aliases {
    my ($hash, $config, $extra) = @_;
    my $result = runaction(\&runaliases, $hash, $config);
    print join "\n", sort @$result;
}

sub cpf {
    my ($machine, $hash, $config, $extra) = @_;
    my @args = split(" ", "$extra");
    my $org = "$machine://$args[0]";
    my $dst = $args[1];
    my $result = runscp($machine, $hash, $config, "$org $dst");
    msg($result);
}

sub cpt {
    my ($machine, $hash, $config, $extra) = @_;
    my @args = split(" ", "$extra");
    my $org = $args[0];
    my $dst = "$machine://$args[1]";
    print ("scp $org $dst");
    my $result = runscp($machine, $hash, $config, "$org $dst");
    msg($result);
}

sub hostnames {
    my ($hash, $config, $extra) = @_;
    my $result = runaction(\&runhostnames, $hash, $config);
    print join "\n", sort @$result;
}

sub info {
    my ($hash, $config, $extra) = @_;
    runaction(\&runinfo, $hash, $config);
}

sub list {
    my ($hash, $config, $extra) = @_;
    my $result = runaction(\&runlist, $hash, $config);
    print join "\n" , sort @$result;
}

sub ping {
    my ($hash, $config, $extra) = @_;
    my $result = runaction(\&runping, $hash, $config);
    msg($result);
}

sub rooms {
    my ($hash, $config, $extra) = @_;
    my @rooms = uniq( map { $hash->{$_}->{room} } keys %$hash);
    print join "\n", sort @rooms;
    return \@rooms;
}

sub ssh {
    my ($hash, $config, $extra) = @_;
    runaction(\&runssh, $hash, $config, $extra);
}

sub testssh {
    my ($hash, $config, $extra) = @_;
    runaction(\&runtestssh, $hash, $config);
}

sub dispatch {
    my ($action, $hash, $config, $extra) = @_;
    debugmsg("Realizando acción $action");
    my $func = $actions{$action};
    if (not $func) {
        say "Acción no implementada: $action. Las acciones disponibles son: " . join " ", keys %actions;
    } else {
        $func->($hash, $config, $extra);
    }
}

my $config = loadperl($configfile);
my ($opt, $usage) = describe_options(
    #"@{[ myname ]} %o <extra>",
    "%c %o <extra>",
    [ 'actions|a',      "lista las posibles acciones", { shortcircuit  => 1 } ],
    [ 'do|d=s',         "acción a realizar", { default  => 'ping' } ],
    [ 'room|r=n',       "número de aula" ],
    [ 'machine|m=s',    "máquina objetivo" ],
    [ 'separator|s=s',  "separador de campos del fichero", { default  => ':' } ],
    [],
    [ 'verbose|v',      "muestra esta ayuda" ],
    [ 'help|h',         "muestra como se usa y termina", { shortcircuit => 1 } ],
);

$DEBUG = 1 if $opt->verbose;
print($usage->text), exit if $opt->help;

my $actions = $opt->actions;
actions, exit if ($actions);
debugmsg(Dumper $config);

my $do = $opt->do;
debugmsg("Acción: $do");
print($usage->text), exit unless $do;

my $filename = "$configdir/rooms.tpl";
my $separator = $opt->separator || ':';
print($usage->text), exit unless $filename;
my $roomnumber = $opt->room;
my $roomkey;
if (defined $roomnumber) {
    debugmsg("Aula: $roomnumber");
    $roomkey = "room";
    if ($roomnumber eq "0") {
        $config->{net} = "127.0";
    }
} else {
    $roomkey = undef;
}
my $hash = readhashfromcsv($filename, "room", $separator, $roomkey, $roomnumber, "machine");
debugmsg(Dumper $hash);
debugmsg("Se han leído " . scalar(keys %$hash) . " entradas.");

my $machine = $opt->machine;
if (defined $machine) {
    debugmsg("Máquina: $machine");;
    my @machines = grep { $hash->{$_}->{machine} eq $machine } keys %$hash;
    my %machines = map { $_ => $hash->{$_} } @machines;
    $hash = \%machines;
}

if ( scalar(%$hash) == 0 && $machine ) {
    komsg("Máquina no encontrada: $machine");
} else {
    my $extra = "@ARGV";
    debugmsg("Extra: $extra") if $extra;

    debugmsg("Fichero de aulas: $filename usando separador: $separator.");

    dispatch($do, $hash, $config, $extra);
}

__END__

=encoding utf8

=head1 NAME

 maquinasaula.pl - Realiza acciones en máquinas remotas conectándose mediante SSH

=head1 SYNOPSIS

=head1 DESCRIPTION

 Se conecta a máquinas de un aula mediante SSH

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2024 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
