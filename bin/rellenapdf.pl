#!/usr/bin/perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.16';

use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use Encode qw(decode encode);
use File::Spec::Functions qw(
    catfile
);

use Date::Calc qw(
    Month_to_Text
    Today
);

use CoreSubs::CoreSubs qw(
    $DEBUG
    debugmsg
    komsg
    mydie
);

use CoreSubs::Proc qw(
    evalperl
);

use CoreSubs::Storage qw(
    absolutepath
    filename
    filepath
);

use Re qw(
    fillpdf
);

use open qw(:utf8);

my %opts = (
            verbose      => 0,
            help         => 0,
            );

#Getopt::Long::Configure('bundling');
GetOptions('v|verbose'       => \$opts{verbose},
           'h|help'          => \$opts{help},
           ) or pod2usage(1);
if ($opts{help}) {
   pod2usage(-exitstatus => 0, -verbose => 2);
}
pod2usage(1) if (@ARGV < 1);
$DEBUG = 1 if $opts{verbose};
my $document = shift;
my $curso = shift;
my $alumno = join " ", @ARGV;
$alumno = decode("utf-8", $alumno);
mydie "$document no es un fichero" if not -f $document;
debugmsg(Dumper(@ARGV));
my $info = catfile(filepath(absolutepath($document)), filename($document)) . ".pl";
debugmsg("Fichero de info: $info");
my $fields = evalperl($info);
debugmsg(Dumper($fields));

$fields->{alumno} = $alumno;
$fields->{curso} = $curso;

$alumno =~ s/ /_/g;
fillpdf($document, "./$alumno-1.pdf", $fields);

__END__

=encoding utf8

=head1 NAME

rellenapdf.pl - Rellena documentos PDF

=head1 SYNOPSIS

 rellenapdf.pl [opciones] fichero_con_campos.pdf Curso Nombre del alumno

 Argumento:
   fichero.pdf Documento a rellenar
 Opciones:
   -h --help      muestra esta ayuda
   -v --verbose   muestra información de depuración
 Resultado:
   Nombre_del_alumno-1.pdf Documento con los campos rellenos

=head1 DESCRIPTION

 Rellena los campos de un documento PDF

=head1 TODO

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2024 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
