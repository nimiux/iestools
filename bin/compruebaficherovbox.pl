#!/usr/bin/env perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.16';

use Data::Dumper;
use Getopt::Long::Descriptive;


use CoreSubs::CoreSubs qw(
    debugmsg
    komsg
    okmsg
);

use CoreSubs::Storage qw(
    maketempfile
);

# simple validation
#$ xmllint --valid example.xml

# validation but without result tree
#$ xmllint --valid --noout example.xml

# validation agains a specific DTD schema
#$ xmllint --noout --dtdvalid <URL> example.xml

# again RelaxNG schema
#$ xmllint --relaxng <schema> example.xml

# again WXS schema
#$ xmllint --schema <schema> example.xml

# again schematron
#$ xmllint --schematron <schema> example.xml

#$ xmllint --shell example.xml

# with cat
#/ > cat //name
# -------
#<name status="on">Develop</name>
# -------
#<name status="off">Sales</name>
# -------
#<name status="on">QA</name>
# -------
#<name status="on">Develop</name>
# -------
#<name status="off">Consultant</name>

# with cat and xpath 
#/ > cat //*[contains(*,"Consultant")] 
# -------
#<team id="032">
#	<name status="off">Consultant</name>
#	<size>3</size>
#</team>
#
## with grep
#/ > grep QA
#/company/branch[2]/team[2]/name : ta-        2 QA

# exit
#/ > exit

#XPath with xmllint

# by root element
#$ xmllint --xpath //company example.xml

# by child elements
#$ xmllint --xpath //name example.xml 

# by specific child element
#$ xmllint --xpath //branch[3] example.xml

# by last child element
#$ xmllint --xpath '/company/branch[last()]' example.xml

# path expression
#$ xmllint --xpath 'child::*/child::*/team' example.xml

# by attribute
#$ xmllint --xpath '//branch[@lang="de-CH"]' example.xml

my %props = (
    'type'    => qw(/VirtualBox/Machine/@OSType),
    'islinux' => qw(/VirtualBox/Machine[@OSType='Linux']),
    'cpu'     => qw(/VirtualBox/Machine/Hardware/CPU),
    'cpu1'    => qw(/VirtualBox/Machine/Hardware/CPU[@count='2']),
    'ram'     => qw(/VirtualBox/Machine/Hardware/Memory[@RAMSize=2048]),
    'vram'    => qw(/VirtualBox/Machine/Hardware/Display[@VRAMSize='32']),
    'display' => qw(//VirtualBox/Machine/Hardware/Display[@VRAMSize=64]),
    'disk1'   => qw(//VirtualBox/Machine/StorageControllers/StorageController[@name=SATA][@PortCount=6]),
    'disk2'   => qw(//VirtualBox/Machine/StorageControllers/StorageController[@name='SATA']/AttachedDevice[@type=DVD][@port=3]),
    'disk3'   => qw(//VirtualBox/Machine/StorageControllers/StorageController[@name='IDE']/AttachedDevice[@type='HardDisk'][@port=1]),
    'disk4'   => qw(//VirtualBox/Machine/StorageControllers/StorageController[@name='SATA']/AttachedDevice[@type='HardDisk'][@port=2]),
    'disk5'   => qw(((((/VirtualBox/Machine/StorageControllers/StorageController[@name='IDE'])/AttachedDevice[@type='HardDisk'])[@nonrotational='true'])[@port=1])[@device=0]"),
    'disk6'   => qw((((/VirtualBox/Machine/StorageControllers/StorageController[@name='IDE'])/AttachedDevice[@type='DVD'])[@port=0])[@device=1]"),
    'disk7'   => qw(((/VirtualBox/Machine/StorageControllers/StorageController[@name='IDE'])/AttachedDevice[@type='HardDisk'])[@port=1]"),
    'disk8'   => qw(((/VirtualBox/Machine/StorageControllers/StorageController[@name='SATA'])/AttachedDevice[@type='DVD'])[@port=3]"),
    'disk9'   => qw(((/VirtualBox/Machine/StorageControllers/StorageController[@name='SATA'])/AttachedDevice[@type='HardDisk'])[@port=2]"),
    'disk10'   => qw(((/VirtualBox/Machine/StorageControllers/StorageController[@name='USB'])/AttachedDevice[@type='HardDisk'])"),
    'sata'     => qw((/VirtualBox/Machine/StorageControllers/StorageController[@name='SATA'])[@PortCount='6']"),
    'net0'     => qw(((/VirtualBox/Machine/Hardware/Network/Adapter)[@slot='1'])[@cable='false']"),
    'net1'     => qw(/VirtualBox/Machine/Hardware/Network/Adapter[@slot=0]/NAT),
    'usb'      => qw((/VirtualBox/Machine/Hardware/USB/Controllers/Controller)[@name='xHCI']),
);

sub VBcheckxpath {
    my ($file, $path) = @_;
    my $tempfile = maketempfile();
    debugmsg("sed -e '/<VirtualBox/s/xmlns.* //' $file > $tempfile");
    system "sed -e '/<VirtualBox/s/xmlns.* //' $file > $tempfile";
    # TODO: call function run_silent for > /dev/null
    debugmsg("Command: xmllint --noout --xpath $path $tempfile &> /dev/null");
    my $result = not system "xmllint --noout --xpath $path $tempfile &> /dev/null";
    $result ? okmsg("OK") : komsg("KO");
}

my ($opt, $usage) = describe_options(
    #"@{[ myname ]} %o <fichero.vbox> <fichero.vb",
    "%c %o <fichero.vbox> <fichero.check>",
    [ 'file|f=s', "fichero con la definición de la máquina virtual VB (.vbox)" ],
    [ 'check|c=s', "fichero con las propiedades que deben cumplirse" ],
    [],
    [ 'verbose|v',  "muestra información extra" ],
    [ 'help|h',       "muestra como se usa y termina", { shortcircuit => 1 } ],
);

my $file = $opt->file || $ARGV[0];
my $check = $opt->check || $ARGV[1];
print($usage->text), exit if $opt->help;
print($usage->text), exit if not $file or not $check;
debugmsg(Dumper(@ARGV));
say("Usando fichero $file y check $check");
# TODO: $file and $check exists?   
VBcheckxpath($file,$props{"type"});
VBcheckxpath($file,$props{"islinux"});
VBcheckxpath($file,$props{"cpu"});
VBcheckxpath($file,$props{"cpu1"});

__END__

=encoding utf8

=head1 NAME
 
compruebaficherovbox.pl - Comprueba ficheros de configuración de VirtualBox.

=head1 SYNOPSIS

 Los ficheros de configuración de VB están en formato XML por lo que la comprobación consiste en buscar una clave dentro del fichero.
 compruebaficherovbox.pl [opciones] <fichero.vbox> <fichero.check>
  [ 'file|f=s', "fichero con la definición de la máquina virtual VB (.vbox)" ],
  [ 'check|c=s', "fichero con las propiedades que deben cumplirse" ],
  [],
  [ 'verbose|v',  "muestra información extra" ],
  [ 'help|h',       "muestra como se usa y termina", { shortcircuit => 1 } ],
);

=head1 DESCRIPTION

 Comprueba que el fichero de configuración de máquina VirtualBox contiene las propiedades definidas en el fichero de comprobación check

 Argumentos:
 fichero.vbox
 fichero.check

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2024 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
