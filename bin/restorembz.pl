#!/usr/bin/perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.16';

use 5.010;
use strict;
use warnings;

use XML::LibXML;

my $filesdir = 'files';
my $filesfile = 'files.xml';

sub newtmpdir {
    my $tmpdir = `mktemp -d`; 
    chomp $tmpdir;
    return $tmpdir;
}

sub restorefile {
    my ($filename, $filepath, $dstdir) = @_;
    #say "Restoring $filepath to \"$dstdir/$filename\"";
    system "mv $filepath \"$dstdir/$filename\"";
}

sub restorembz {
    my ($infile) = @_;
    my ($contenthash, $filepath);
    my ($unpackdir, $dstdir) = (newtmpdir, newtmpdir);
    system "tar -C $unpackdir -xf $infile";
    my $dom = XML::LibXML->load_xml(location => "$unpackdir/$filesfile");
    foreach my $file ($dom->findnodes('/files/file')) {
        $contenthash = $file->findvalue('./contenthash');
        $filepath = "$unpackdir/$filesdir/${\substr($contenthash, 0, 2)}/$contenthash";
        restorefile($file->findvalue('./filename'), $filepath, $dstdir) if -e $filepath;
    }
    return $dstdir;
}

my $mbzfile = shift @ARGV;
say("Use: $0 filename.mbz"), exit if not $mbzfile;
my $dstdir = restorembz $mbzfile;
say "mbz file restored to $dstdir";
system "ls $dstdir";

__END__

=encoding utf8

=head1 NAME
 
restorembz.pl --- Desempaqueta un fichero en formato mbz de Moodle

=head1 SYNOPSIS

 restorembz.pl nombredefichero.mbz

=head1 DESCRIPTION

 Genera un fichero de usuarios y contraseñas con información aleatoria.

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2024 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
