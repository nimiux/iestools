#!/bin/bash

PROGRAM="maquinasaula.pl"
INTERVAL=10
BANNER="------------------------------------------------------"

roomnumber=$1
shift
extra="$@"
echo "Extra: $extra"

usage () {
    echo "Usage: $0 [roomnumber] \"inline script\""
}

doit () {
    roomnumber=$1
    machine=$2
    shift 2
    extra=$@
    echo "Lanzando comandos en máquina ${machine}"
    runit "${outdir}/${machine}" "${PROGRAM} -d ssh -r ${roomnumber} -m ${machine} \"${extra}\""
}

runit () {
    file=$1
    shift 1 
    commands=$@
    date >> $file
    echo "Running: $commands" >> $file
    echo ${BANNER} >> $file
    eval ${commands} | tee -a $file 
    echo ${BANNER} >> $file
}

[[ -z ${roomnumber} ]] && usage && exit 1
echo "Grabando Aula: ${roomnumber}. Acciones: ${extra}"
machines=$(${PROGRAM} -d machines -r ${roomnumber})
outdir="$(mktemp -d -p /tmp SOM-${roomnumber}-XXXXXXXXXX)"
echo "Guardando registro en directorio ${outdir}"

while : ; do
    
    for machine in ${machines} ; do
        doit ${roomnumber} ${machine} ${extra}
    done
    sleep ${INTERVAL}
done
