#!/usr/bin/env bash

roomfile="${IESTOOLSCONFIGDIR}/rooms.tpl"

getrooms() {
    grep -v "^#" $roomfile | tail -n +2 | cut -f 1 -d: | uniq | sort
}

#getmachines() {
    #room=${COMP_WORDS[2]}
    #grep "^$room:" $roomfile | cut -f 3 -d: | uniq | sort
#}

_som_completions()
{
    local opts
    actions=$(maquinasaula.pl -a)
    case $COMP_CWORD in
        1)
            COMPREPLY=( $(compgen -W "${actions}" -- "${COMP_WORDS[COMP_CWORD]}") )
            ;;
        2)
            #rooms=$(maquinasaula.pl -d rooms)
	    rooms=$(getrooms)
            COMPREPLY=( $(compgen -W "${rooms}" -- "${COMP_WORDS[COMP_CWORD]}") )
            ;;
        3)
            machines=$(getmachines)
            COMPREPLY=( $(compgen -W "${machines}" -- "${COMP_WORDS[COMP_CWORD]}") )
            ;;
    esac
    return 0
}
complete -F _som_completions som

