#!/bin/bash

action=$1
shift
roomnumber=$1
shift
machine=$1
shift
extra="$@"

usage () {
    echo "Usage: $0 action [roomnumber] [machine]"
    echo "Actions:"
    maquinasaula.pl -a
}

[[ -z $action ]] && usage && exit 1
[[ -n $roomnumber ]] && roomnumber="-r $roomnumber"
[[ -n $machine ]] && machine="-m $machine"
command="maquinasaula.pl -d $action $roomnumber $machine -- $extra"
[[ -n $DEBUG ]] && echo $command
$command
