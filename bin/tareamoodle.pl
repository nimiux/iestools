#!/usr/bin/env perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

#use Archive::Extract;
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Slurp;
use File::Spec::Functions qw(
    catfile
);
use File::Temp qw(
    tempdir
);
use Getopt::Long::Descriptive;
use Sub::Identify qw(
    sub_name
);

use IESTools qw(
    showfile
    showfolder
);

use CoreSubs::CoreSubs qw(
    $DEBUG
    bluemsg
    debugmsg
    donemsg
    failmsg
    mydie
);

use CoreSubs::Proc qw(
    loadperl
);

use CoreSubs::Storage qw(
    extract
    filetype
    walk
);

=head1 NAME

tareamoodle.pl - Desempaqueta una tarea de Moodle y la coteja con una plantilla para comprobar si se adapta a ella

=cut

=head1 SYNOPSIS

=cut

=head1 DESCRIPTION

Desempaqueta una tarea de Moodle y la coteja con una plantilla para comprobar si se adapta a ella

Argumentos:

1 Plantilla en formato mat describiendo la estructura del material de la tarea
2 Fichero en formato zip que contiene la tarea Moodle

=cut

sub unpack_assignment {
    my ($assignmentfile) = @_;
    my $tmpdir = tempdir();
    my $path;
    my $files;
    my $tasks = extract($assignmentfile, $tmpdir);
    debugmsg(Dumper($tasks));
    for (@{$tasks}) {
        debugmsg("Procesando tarea $_");
        $path = "$tmpdir/$_";
        #if ($path =~ m/.zip$/) {
        if (filetype($path) eq 'zip') {
            extract($path, dirname($path));
            debugmsg("Eliminando fichero $path");
            system "rm \"$path\"";
        }
    }
    bluemsg("Tareas desempaquetadas en: $tmpdir");
    return $tmpdir;
}

sub find_item {
    my ($itemname, $path, $itemext, $itemhash) = @_;
    my $itempath = catfile($path, $itemname . $itemext);
    my $key = "*" . $itemext;
    debugmsg("Buscando item: $itempath con clave $key");
    if (exists $itemhash->{$key}) {
        $itemhash = $itemhash->{$key};
        donemsg("Encontrada entrada: $key para $itemname");
        my $itemformat = $itemhash->{format};
        my $itemtype = filetype($itempath);
        if ($itemtype ne $itemformat) {
            failmsg("$itemname existe pero es un $itemtype y debería ser un $itemformat");
        } else {
            my $funcref = $itemhash->{contents};
            $funcref->($itempath, $itemhash);
        }
    } else {
        failmsg("No se solicitaba $itemname en la tarea");
    }
}

sub check_item {
    my ($item, $itemhash, $itempath) = @_;
    debugmsg("Item: $item. Itempath: $itempath.");
    debugmsg(Dumper($itemhash));
    my $itemformat = $itemhash->{format};
    my $funcref = $itemhash->{contents};
    my ($itemname, $path, $itemext) = fileparse($item, '\.[^\.]*');
    $itempath = catfile($itempath, $item);
    my $itemtype = filetype($itempath);
    debugmsg("Comprobando fichero: $itempath");
    if ($itemtype eq 'empty') {
        failmsg("$item existe pero está vacío");
    } else {
        if ($itemtype ne $itemformat) {
            failmsg("$item existe pero es un $itemtype y debería ser un $itemformat");
        } else {
            donemsg("$item existe y es de tipo $itemtype");
            $funcref->($itempath, $itemhash);
        }
    }
}

sub check_folder {
    my ($folderpath, $template) = @_;
    my %template = %{$template};
    debugmsg("Comprobando carpeta. $folderpath");
    debugmsg(Dumper($template));
    my @items = read_dir($folderpath);
    @items = sort {"\L$a" cmp "\L$b"} @items;
    foreach my $item (@items) {
        next if ($item =~ /^\.$/);
        next if ($item =~ /^\.\.$/);
        my $itemfullpath = catfile($folderpath, $item);
        debugmsg("Comprobando item $item");
        my ($itemname, $path, $itemext) = fileparse($itemfullpath, '\.[^\.]*');
        my $key = $itemname . lc $itemext;
        debugmsg("itemname: $itemname, path: $path. itemext: $itemext");
        debugmsg("Comprobando key: $key");
        if (not exists $template{$key}) {
            find_item($itemname, $path, $itemext, $template);
        } else {
            my $itementry = $template{$key};
            if (exists $itementry->{type}) {
                if ($itementry->{type} eq 'folder') {
                    if (not -d $itemfullpath) {
                        failmsg("$item existe pero no es una carpeta");
                    } else {
                        my $contents = $itementry->{contents};
                        if (defined $contents) {
                            if (ref($contents) eq "CODE") {
                                 donemsg("Se aplica la función ${\sub_name($contents)} al ítem $item");
                                 $contents->($item, $folderpath);
                                 # Using dispatcher
                                 #my %actions = (
                                 #    'SHOWFILE' => \&showfile,
                                 #    'SHOWFOLDER' => \&showfolder,
                                 #);
                                 #    if (!ref($contents)) {
                                 #        my $funcrec = $actions{$contents};
                                 #        if (!defined $funcrec) {
                                 #            failmsg("No existe función para $contents");
                                 #        } else {
                                 #            donemsg("Se aplica la función ${\sub_name($funcrec)} al ítem $item");
                                 #            $funcrec->($item, $folderpath);
                                 #        }
                            } else {
	                        check_folder(catfile($folderpath, $item), $itementry->{contents});
                            }
                        } else {
                            donemsg("No se comprueba el contenido de la carpeta $item");
                        }
                    }
                } else {
                    check_item($item, $itementry, $folderpath);
                }
            } else {
                check_item($item, $itementry, $folderpath);
            }
        }
    }
}

sub check_task {
    my ($taskpath, @extra) = @_;
    my $template = $extra[0];
    bluemsg("Comprobando tarea de $taskpath");
    check_folder($taskpath, $template);
    bluemsg("Pulsar intro para la siguiente tarea");
    <STDIN>;
}

my ($opt, $usage) = describe_options(
  "%c %o <plantilla.mat> <ficherotareaMoodle.zip>",
  [],
  [ 'verbose|v',  "muestra información extra" ],
  [ 'help|h',       "muestra como se usa y termina", { shortcircuit => 1 } ],
);
my ($tasktemplatefile, $taskfile) = @ARGV;
debugmsg(Dumper(@ARGV));
print($usage->text), exit if $opt->help;
print($usage->text), exit if not defined $tasktemplatefile or not defined $taskfile;
mydie "$tasktemplatefile no es un fichero" if not -f $tasktemplatefile;
mydie "$taskfile no es un fichero" if not -f $taskfile;
$DEBUG = 1 if $opt->verbose;
my $template = loadperl($tasktemplatefile);
debugmsg(Dumper($template));
mydie "No se puede cargar la plantilla desde el fichero $tasktemplatefile" if (not defined $template);
debugmsg("Usando plantilla:");
debugmsg(Dumper($template));
my $tmppath = unpack_assignment $taskfile;
debugmsg("Tareas desempaquetadas en $tmppath");
walk($tmppath, \&check_task, 0, $template);

__END__

=encoding utf8

=head1 NAME

tareamoodle.pl - Desempaqueta una tarea descargada desde un curso de la plataforma Moodle

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 TODO
    * Task shoud be presented in correct order, á comes after a not at the end. check walf sub
    * Use walk in check_folder
    * Some zip files containing files with non ascii chars can't be displayed:
                Instalaciвn de LibreOffice.GIF

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2024 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
