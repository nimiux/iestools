#!/usr/bin/perl

package main;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.16';

use Data::Dumper;
use Encode;
use File::Slurp;
use Getopt::Long::Descriptive;
use Text::CSV;
use Text::Template;

our $VERSION = '0.1';

use CoreSubs::CoreSubs qw(
    $DEBUG
    debugmsg
    komsg
    mydie
);

use CoreSubs::Storage qw(
    readfromterminal
    readhashfromcsv
    readpassword
);

use CoreSubs::Net qw(
    send_mail
);
use CoreSubs::Proc qw(
    loadperl
);

use open qw(:std :utf8);

my $home = $ENV{"HOME"};
my $CONFIGDIRVAR = "IESTOOLSCONFIGDIR";
my $configdir = $ENV{$CONFIGDIRVAR} || "";
komsg("Variable de entorno $CONFIGDIRVAR no definida"), exit if not $configdir;
my $configfile = "$configdir/config.pl";
my $config = loadperl($configfile);
my $smtppars = $config->{mta};
my ($user, $tutor, $server) = ($smtppars->{user}, $config->{tutor}, $config->{servidor});
my $myemail = "$user\@$server";
my $from = "$tutor <$myemail>";
my $cc = $from;
my $signature = << "EOT";
---
$tutor (Tutor de $config->{grupo})
$myemail
$config->{departamento}
$config->{centro}, $config->{ciudad} ($config->{correocentro})
EOT
    
sub composemsg {
    my ($template, $vars) = @_;
    my $message = Text::Template->new(TYPE => 'STRING', SOURCE => $template);
    my $rendered = $message->fill_in(HASH => $vars);
    $rendered .= $signature;
    debugmsg("Message is: $rendered");
    return $rendered;
}

my ($opt, $usage) = describe_options(
    "%c %o <destinatarios.csv> [ <mensaje.txt> ]",
    [ 'subject|s=s',    "asunto del mensaje" ],
    [],
    [ 'verbose|v',      "muestra información extra" ],
    [ 'help|h',         "muestra como se usa y termina", { shortcircuit => 1 } ],
);

my ($recipientsfile, $msgfile) = @ARGV;
$DEBUG = 1 if $opt->verbose;
debugmsg(Dumper(@ARGV));
print($usage->text), exit if $opt->help;
print($usage->text), exit if not defined $recipientsfile;
mydie "$recipientsfile no es un fichero" if not -f $recipientsfile;
my $recipientshash = readhashfromcsv($recipientsfile, "name");
debugmsg(Dumper($recipientshash));
my $subject;
if (not defined $opt->subject) {
    say "No se ha indicado asunto: ";
    $subject = readfromterminal();
} else {
    $subject = $opt->subject;
}
my $text;
if (not defined $msgfile) {
    say "No se ha indicado fichero de mensaje";
    $text = readfromterminal(1);
} elsif (not -f $msgfile) {
    say "No se ha encontrado el fichero de mensaje: $msgfile";
    $text = readfromterminal(1);
} else {
    $text = read_file($msgfile);
}
say "Contraseña del MTA";
my $password = readpassword;
chomp($password);
foreach my $recipientindex (keys %$recipientshash) {
    my $recipient = $recipientshash->{$recipientindex};
    my $email = $recipient->{email};
    say "Enviando mensaje a: $recipient->{surname}, $recipient->{name} ($recipient->{email})";
    $text = composemsg($text, $recipient);
    send_mail($from, $email, $cc, $subject, $text, $smtppars, $password);
    say "Mensaje enviado";
    sleep($config->{intervalo});
}

__END__

=encoding utf8

=head1 NAME

enviamensaje.pl - Envía un mensaje a múltiples destinatarios

=head1 SYNOPSIS

 enviamensaje.pl [opciones] ficherorecipientes.csv

      -opt --long      Option description

 Realiza envíos a alumnos con información incluída en un fichero CSV
   Argumento:
     destinatarios.csv Ruta al fichero CSV con los datos de los alumnos:
        name:surname:gender:email:field1:field2:...:fieldN
          gender: a: femenin o: masculin
   Opciones
     -v --verbose  muestra información de depuración
     -s --subject  asunto que se indicará en los mensajes
     -p --pass     contraseña de la cuenta de correo electrónico de EducaMadrid

=head1 DESCRIPTION

 Envía mensajes de correo electrónico

=head1 TODO

enviacalificaciones           Parametrizar convocatoria ordinaria/extraordinaria

=head1 VERSION

This man page documents IESTools version 1.0.

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2024 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
