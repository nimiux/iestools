#!/bin/bash
[ $(id -u) -ne 0 ] && echo "Debes ser root" && exit 1
cleanit () {
    apt -y autoremove aisleriot \
                      baobab \
                      bluez \
                      bluez-obexd \
                      bolt \
                      brasero-common \
                      cdrdao \
                      cheese \
                      cups \
                      cups-common \
                      cups-pk-helper \
                      evolution \
                      gnome-bluetooth \
                      gnome-calculator \
                      gnome-calendar \
                      gnome-chess \
                      gnome-clocks \
                      gnome-contacts \
                      gnome-games \
                      gnome-klotski \
                      gnome-mahjongg \
                      gnome-maps \
                      gnome-mines \
                      gnome-music \
                      gnome-nibbles \
                      gnome-robots \
                      gnome-sound-recorder \
                      gnome-sudoku \
                      gnome-sushi \
                      gnome-taquin \
                      gnome-tetravex \
                      gnome-weather \
                      imagemagick-6-common \
                      iw \
                      lightsoff \
                      quadrapassel \
                      rhythmbox \
                      sane-airscan \
                      sane-utils \
                      seahorse \
                      shotwell \
                      simple-scan \
                      tali \
                      totem \
                      transmission-gtk
    apt autoremove
    apt clean
    journalctl --vacuum-time 1w
    rm -r /var/lib/apt/lists/*
}

cleanit
