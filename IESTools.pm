package IESTools;

use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf-8)';
use feature ':5.32';

require Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
    $VERSION
    showfile
    showfolder
);

our $VERSION = '1.0';

use CoreSubs::CoreSubs qw(
    debugmsg
    failmsg
);

use CoreSubs::Proc qw(
);

use CoreSubs::Storage qw(
    fileext
    walk
);

use Data::Dumper;

my $apps = {
    'gif' => 'display',
    'jpg' => 'display',
    'odt' => 'libreoffice',
    'pdf' => 'mupdf',
    'png' => 'display',
    'txt' => 'gedit',
    'vbox' => 'VirtualBox',
};

# filedata is a hash:
#   'format'  the format of the file
#   'type'    the type of data expected to be in the file
sub showfile {
    my ($filepath, $filedata) = @_;
    my $fileformat = fileext($filepath);
    $fileformat = $filedata->{format} if exists $filedata->{format};
    $fileformat = $filedata->{type} if exists $filedata->{type};
    $fileformat = lc $fileformat;
    debugmsg("Mostrando fichero: $filepath. Formato: $fileformat");
    debugmsg(Dumper($filepath, $filedata));
    my $app = $apps->{$fileformat};
    if (not defined $app) {
        failmsg("No puedo encontrar aplicación para el tipo $fileformat");
    } else {
        debugmsg("Usando la aplicación $app para el formato: $fileformat");
        my $command = "$app \"$filepath\"";
        debugmsg("Lanzando: $command");
        system("$command");
    }
}

sub showfolder {
    my ($foldername, $folderpath) = @_;
    my $folder = "$folderpath/$foldername";
    debugmsg("Showing contents of $folder");
    # TODO. Use walk
    walk($folder, \&showfile, 0);
    #opendir(DH, $folder);
    #my @items = readdir(DH);
    #closedir(DH);
    #foreach my $item (@items) {
    #    next if ($item =~ /^\.$/);
    #    next if ($item =~ /^\.\.$/);
    #    $item = catfile($folder, $item);
    #}
}

__END__

=encoding utf8

=head1 NAME

IESTools - Herramientas para facilitar las labores de docencia

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR

José María Alonso Josa, E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2018-2025 by José María Alonso Josa

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://codeberg.org/nimiux/iestools/issues

=cut
